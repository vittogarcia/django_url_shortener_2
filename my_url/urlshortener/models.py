from django.db import models
from django.forms import model_to_dict
from .utils import create_shortened_url

# Create your models here.
class Shortener(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    long_url = models.URLField()
    short_url = models.CharField(max_length=20, unique=True, blank=True)
    times_clicked = models.PositiveIntegerField(default=0)

    def __str__(self):
        return f'{self.long_url} to {self.short_url}'
    
    def to_json(self):
        item = model_to_dict(self)
        return item
    
    class Meta:
        ordering = ['-created']

    def save(self, *args, **kwargs):
        if not self.short_url:
            self.short_url = create_shortened_url(self)
        super().save(*args, **kwargs)

class DetailShortener(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    browser = models.CharField(max_length=100, default='')
    url = models.ForeignKey(Shortener, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.url.short_url} from {self.browser}'
    
    def to_json(self):
        item = model_to_dict(self)
        return item
    
    class Meta:
        ordering = ['-created']
    
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
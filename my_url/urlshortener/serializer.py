from .models import Shortener
from rest_framework import viewsets, permissions, serializers

class ShortenerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shortener
        fields = '__all__'
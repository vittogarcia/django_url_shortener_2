from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework.utils import json
from .models import Shortener

class ShortenerListTestCase(APITestCase):
    api_path = 'api/'
    request_payload = {
        'task': 'Run Test'
    }

    def get_shortener(self):
        response = self.client.post(self.api_path, self.request_payload, format=json)
        response_payload = json.loads(response.content)
        status_code = response.status_code
        return status_code, response_payload
    
    def test_get_shortener(self):
        count_shortener = Shortener.objects.count()
        status_code, response_payload = self.get_shortener()

        self.assertEqual(status_code, 201)
from django.urls import path
from .views import home_view, redirect_url_view, full_list, shortener_json, details, shortener_to_json

app_name="shortener"
urlpatterns = [
    path(
        '',
        home_view,
        name="home"
    ),
    path(
        'full_list/',
        full_list,
        name="full_list"
    ),
    path(
        '<str:short_url>',
        redirect_url_view,
        name='redirect'
    ),
    path(
        'details/<int:pk>/',
        details,
        name='details'
    ),
    path(
        'shortener_json/',
        shortener_to_json,
        name='shortener_json'
    ),
    path(
        'api/',
        shortener_json
    )
]

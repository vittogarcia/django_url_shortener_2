import datetime
from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
#Serializer
from rest_framework.exceptions import NotFound
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from .models import Shortener, DetailShortener
from .forms import ShortenerForm
from .serializer import ShortenerSerializer

def home_view(request):
    template = 'home.html'
    context = {}
    context['form'] = ShortenerForm()

    if request.method == 'GET':
        context['full_list'] = Shortener.objects.all()

        return render(request, template, context)
    elif request.method == 'POST':
        my_form=ShortenerForm(request.POST)
        if my_form.is_valid():
            url_object = my_form.save()
            new_url = request.build_absolute_uri('/')+url_object.short_url
            long_url = url_object.long_url
            context['new_url'] = new_url
            context['long_url'] = long_url
        context['errors'] = my_form.errors

        return render(request, template, context)

def full_list(request):
    template = 'full_list.html'
    context = {}
    context['full_list'] = Shortener.objects.all()

    return render(request, template, context)

def redirect_url_view(request, short_url):
    try:
        shortener = get_object_or_404(Shortener, short_url=short_url)
        shortener.times_clicked += 1
        shortener.save()
        user_agent = request.user_agent
        detail = DetailShortener(
            browser = user_agent.browser.family + ' ' + user_agent.browser.version_string,
            url_id = shortener.pk
        )
        detail.save()
        return HttpResponseRedirect(shortener.long_url)
    except:
        return render(request, '404.html', status=404)

def details(request, pk):
    template = 'details.html'
    context = {}
    
    try:
        shortener = Shortener.objects.get(pk=pk)
        details = DetailShortener.objects.filter(url_id=shortener.pk)
        context['url'] = shortener.long_url
        for d in details:
            print(d)
        context['details_url'] = details

        return render(request, template, context)
    except:
        return render(request, '404.html', status=404)

def shortener_to_json(request):
    data = {}
    if request.method == 'GET':
        data['data'] = []
        shortener = Shortener.objects.all().order_by('-id')[:10]
        for s in shortener:
            item = {
                'type': 'url',
                'id': s.id,
                'attributes': {
                    'created_at': s.created,
                    'original_url': s.long_url,
                    'url': request.build_absolute_uri('/') + s.short_url,
                    'clicks': s.times_clicked
                }
            }
            data['data'].append(item)
            item['relationships'] = []
            for d in DetailShortener.objects.filter(url_id=s.pk):
                item['relationships'].append({
                    'id': d.pk,
                    'browser': d.browser
                })
        return JsonResponse(data, safe=False)

@api_view(['GET'])
def shortener_json(request):
    if request.method == 'GET':
        data = []
        shortener = Shortener.objects.all().order_by('-id')[:10]
        for s in shortener:
            data.append(
                {
                    'type': 'url',
                    'id': s.pk,
                    'attributes': {
                        'created-at': s.created,
                        'original-url': s.long_url,
                        'url': s.short_url,
                        'clicks': s.times_clicked
                    },
                    'relationships': {
                        'metrics': {
                            'data': [
                                {
                                    'id': 'hola'
                                }
                            ]
                        }
                    }
                }
            )
        print(data)

        serializer = ShortenerSerializer(shortener, many=True)
        return JsonResponse(serializer.data, safe=False)
